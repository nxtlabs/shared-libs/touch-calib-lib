## Usage

Copy `touch-calib.c` and `touch-calib.h` to your project or add this repository as a git submodule
* `update_calib_matrix(...)` - builds calibration matrix from three display points and three corresponding touch points
* `get_display_point(...)` - transforms touch points to display points

## Demo

Review calibration points inside of `test.c` and then run

* `$ make`

Use `test.svg` as a reference.

## Documentation

* [wiki](https://gitlab.com/nxtlabs/touch-calib-lib/-/wikis/home)
