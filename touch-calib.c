#include <stdio.h>  // FILE

#include "touch-calib.h"

/**
 * @param[in] display - pointer to an array of three sample, reference points
 * @param[in] touch - pointer to the array of touch points corresponding to the reference display points
 * @param[out] matrix - pointer to the calibration matrix computed for the set of points being provided
 */
int update_calib_matrix(point_t* display, point_t* touch, matrix_t* matrix) {
#ifdef TOUCH_CALIB_SOFTFP
  matrix->divider =
      ((int64_t)(touch[0].x - touch[2].x) * (touch[1].y - touch[2].y)) -
      ((int64_t)(touch[1].x - touch[2].x) * (touch[0].y - touch[2].y));

  if (matrix->divider == 0) {
    return NOT_OK;
  }

  matrix->an =
      ((int64_t)(display[0].x - display[2].x) * (touch[1].y - touch[2].y)) -
      ((int64_t)(display[1].x - display[2].x) * (touch[0].y - touch[2].y));

  matrix->bn =
      ((int64_t)(touch[0].x - touch[2].x) * (display[1].x - display[2].x)) -
      ((int64_t)(display[0].x - display[2].x) * (touch[1].x - touch[2].x));

  matrix->cn =
      ((int64_t)(touch[2].x) * display[1].x - touch[1].x * display[2].x) * touch[0].y +
      ((int64_t)(touch[0].x) * display[2].x - touch[2].x * display[0].x) * touch[1].y +
      ((int64_t)(touch[1].x) * display[0].x - touch[0].x * display[1].x) * touch[2].y;

  matrix->dn =
      ((int64_t)(display[0].y - display[2].y) * (touch[1].y - touch[2].y)) -
      ((int64_t)(display[1].y - display[2].y) * (touch[0].y - touch[2].y));

  matrix->en =
      ((int64_t)(touch[0].x - touch[2].x) * (display[1].y - display[2].y)) -
      ((int64_t)(display[0].y - display[2].y) * (touch[1].x - touch[2].x));

  matrix->fn =
      ((int64_t)(touch[2].x) * display[1].y - touch[1].x * display[2].y) * touch[0].y +
      ((int64_t)(touch[0].x) * display[2].y - touch[2].x * display[0].y) * touch[1].y +
      ((int64_t)(touch[1].x) * display[0].y - touch[0].x * display[1].y) * touch[2].y;
#else
  float divider =
      ((touch[0].x - touch[2].x) * (touch[1].y - touch[2].y)) -
      ((touch[1].x - touch[2].x) * (touch[0].y - touch[2].y));

  if (divider == INFINITY) {
    return NOT_OK;
  }

  matrix->an =
      ((display[0].x - display[2].x) * (touch[1].y - touch[2].y)) -
      ((display[1].x - display[2].x) * (touch[0].y - touch[2].y));

  matrix->bn =
      ((touch[0].x - touch[2].x) * (display[1].x - display[2].x)) -
      ((display[0].x - display[2].x) * (touch[1].x - touch[2].x));

  matrix->cn =
      (touch[2].x * display[1].x - touch[1].x * display[2].x) * touch[0].y +
      (touch[0].x * display[2].x - touch[2].x * display[0].x) * touch[1].y +
      (touch[1].x * display[0].x - touch[0].x * display[1].x) * touch[2].y;

  matrix->dn =
      ((display[0].y - display[2].y) * (touch[1].y - touch[2].y)) -
      ((display[1].y - display[2].y) * (touch[0].y - touch[2].y));

  matrix->en =
      ((touch[0].x - touch[2].x) * (display[1].y - display[2].y)) -
      ((display[0].y - display[2].y) * (touch[1].x - touch[2].x));

  matrix->fn =
      (touch[2].x * display[1].y - touch[1].x * display[2].y) * touch[0].y +
      (touch[0].x * display[2].y - touch[2].x * display[0].y) * touch[1].y +
      (touch[1].x * display[0].y - touch[0].x * display[1].y) * touch[2].y;

  matrix->an /= divider;
  matrix->bn /= divider;
  matrix->cn /= divider;
  matrix->dn /= divider;
  matrix->en /= divider;
  matrix->fn /= divider;
#endif

  return OK;
}

/**
 * Transforms value from touch to display coordinates.
 * 
 * @param[in] matrix - pointer to calibration factors matrix previously calculated by `update_calibration_matrix()`
 * @param[in] touch - pointer to the reported touch point
 * @param[out] display - pointer to the calculated point on the display
 */
int get_display_point(matrix_t* matrix, point_t* touch, point_t* display) {
#ifdef TOUCH_CALIB_SOFTFP
  if (matrix->divider == 0) {
    return NOT_OK;
  }

  display->x = ((matrix->an * touch->x) + (matrix->bn * touch->y) + matrix->cn) / matrix->divider;
  display->y = ((matrix->dn * touch->x) + (matrix->en * touch->y) + matrix->fn) / matrix->divider;
#else
  display->x = ((matrix->an * touch->x) + (matrix->bn * touch->y) + matrix->cn);
  display->y = ((matrix->dn * touch->x) + (matrix->en * touch->y) + matrix->fn);
#endif

  return OK;
}

/**
 * Saves calibration matrix to selected location
 * 
 * @param[in] matrix - pointer to calibration factors matrix
 */
int save_calib_matrix(matrix_t* matrix, char* config_path) {
  FILE* fp = fopen(config_path, "w");

  if (fp == NULL) {
    return NOT_OK;
  }

  fwrite(&(matrix->an), 1, sizeof(matrix->an), fp);
  fwrite(&(matrix->bn), 1, sizeof(matrix->bn), fp);
  fwrite(&(matrix->cn), 1, sizeof(matrix->cn), fp);
  fwrite(&(matrix->dn), 1, sizeof(matrix->dn), fp);
  fwrite(&(matrix->en), 1, sizeof(matrix->en), fp);
  fwrite(&(matrix->fn), 1, sizeof(matrix->fn), fp);

  fclose(fp);

  return OK;
}

/**
 * Loads calibration matrix from selected location
 * 
 * @param[in] matrix - pointer to calibration factors matrix
 */
int load_calib_matrix(matrix_t* matrix, char* config_path) {
  FILE* fp = fopen(config_path, "r");

  if (fp == NULL) {
    return NOT_OK;
  }

  fread(&(matrix->an), 1, sizeof(matrix->an), fp);
  fread(&(matrix->bn), 1, sizeof(matrix->bn), fp);
  fread(&(matrix->cn), 1, sizeof(matrix->cn), fp);
  fread(&(matrix->dn), 1, sizeof(matrix->dn), fp);
  fread(&(matrix->en), 1, sizeof(matrix->en), fp);
  fread(&(matrix->fn), 1, sizeof(matrix->fn), fp);

  fclose(fp);

  return OK;
}
