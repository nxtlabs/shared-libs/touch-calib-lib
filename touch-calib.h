#ifndef __TOUCH_CALIB_H
#define __TOUCH_CALIB_H

#include <stdint.h>
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OK
#  define OK 0
#endif

#ifndef NOT_OK
#  define NOT_OK -1
#endif

typedef struct {
  int32_t x, y;
} point_t;

typedef struct {
#ifdef TOUCH_CALIB_SOFTFP
  int64_t an, bn, cn, dn, en, fn, divider;
#else
  float an, bn, cn, dn, en, fn;
#endif
} matrix_t;

int update_calib_matrix(point_t* display_ref, point_t* touch, matrix_t* matrix);
int get_display_point(matrix_t* matrix, point_t* touch, point_t* display);
int save_calib_matrix(matrix_t* matrix, char* config_path);
int load_calib_matrix(matrix_t* matrix, char* config_path);

#ifdef __cplusplus
}
#endif

#endif
