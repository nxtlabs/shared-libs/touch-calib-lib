#include <stdio.h>

#include "touch-calib.h"

#define MAX_SAMPLES 10

#ifdef TEST

static void test_points(matrix_t* matrix) {
  point_t display;

  point_t touch_samples[MAX_SAMPLES] = {
    { 3073, 2930 }, { 1205, 2515 }, { 2847, 1533 },  // first three points are the same as used for calibration
    { 553, 4054 },  // top-left
    { 4062, 3487 },  // top-right
    { 3611, 694 },  // bottom-right
    { 103, 1260 },  // bottom-left
    { 4096, 4096 },  // the highest possible value
    { 0, 4096 },  // the highest possible value
    { 4096, 0 }  // the highest possible value
  };

  printf("\n       Touch              Display\n\n");

  for (int n = 0; n < MAX_SAMPLES; ++n) {
    get_display_point(matrix, &touch_samples[n], &display);

    printf(" %8d, %-8d  %8d, %-8d\n",
        touch_samples[n].x, touch_samples[n].y, display.x, display.y);
  }
}

int main(int argc, char** argv) {
  /* In our example screen resolution is 480x320, touch resolution 4096x4096.
    Calibration points picked arbitrarily (75%, 75%), (25%, 50%), (75%, 25%) */

  point_t display_ref_samples[] = { { 360, 240 }, { 120, 160 }, { 360, 80 } };
  point_t touch_ref_samples[] = { { 3073, 2930 }, { 1205, 2515 }, { 2847, 1533 } };
  matrix_t matrix;

  update_calib_matrix(&display_ref_samples[0], &touch_ref_samples[0], &matrix);

  test_points(&matrix);

  return OK;
}

#endif
